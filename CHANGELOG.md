## [1.3.1](https://gitlab.com/extsidvind/sandbox-semantic-release/compare/v1.3.0...v1.3.1) (2019-07-31)


### Bug Fixes

* **add:** support varargs ([e14be02](https://gitlab.com/extsidvind/sandbox-semantic-release/commit/e14be02)), closes [#1](https://gitlab.com/extsidvind/sandbox-semantic-release/issues/1)

# [1.3.0](https://gitlab.com/extsidvind/sandbox-semantic-release/compare/v1.2.0...v1.3.0) (2019-07-31)


### Features

* **add:** added forth param ([748e557](https://gitlab.com/extsidvind/sandbox-semantic-release/commit/748e557))

# [1.2.0](https://gitlab.com/extsidvind/sandbox-semantic-release/compare/v1.1.0...v1.2.0) (2019-07-31)


### Features

* **add:** added documentation ([2f71806](https://gitlab.com/extsidvind/sandbox-semantic-release/commit/2f71806))

# [1.1.0](https://gitlab.com/extsidvind/sandbox-semantic-release/compare/v1.0.0...v1.1.0) (2019-07-30)


### Features

* **foo:** support three operators ([6126e25](https://gitlab.com/extsidvind/sandbox-semantic-release/commit/6126e25))

# 1.0.0 (2019-07-30)


### Features

* **foo:** support addition ([3f75f3a](https://gitlab.com/extsidvind/sandbox-semantic-release/commit/3f75f3a))
