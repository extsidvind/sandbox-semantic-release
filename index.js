/* add numbers */
module.exports = function add(...values) {
  return values.reduce((sum, cur) => sum + cur, 0);
};
